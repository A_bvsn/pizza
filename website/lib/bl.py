from website.models import Dish, OrderItems, Cart, Order
from website.lib.cart import cart_session
from website.forms import OrderForm


def fetch_all_dishes():
    return Dish.objects.all().order_by('id')


def fetch_cart_item(dish_id):
    return OrderItems.objects.filter(dish_id=dish_id).first()


def get_cart_items(cart):
    return cart.ordered_items.all().select_related('dish')


def get_cart_total(cart_items):
    return sum([item.dish.price * item.qty for item in cart_items])


def add_item(cart, dish_id):
    dish_id = int(dish_id)
    (order_dish, _) = OrderItems.objects.get_or_create(dish_id=dish_id)
    if order_dish not in cart.ordered_items.all():
        cart.ordered_items.add(order_dish)
        cart.save()
    else:
        incr_qty_in_cart(cart, dish_id)
    return cart


def remove_item(cart, dish_id):
    # TODO: Try-except for NoneType object
    dish_id = int(dish_id)
    cart_item = fetch_cart_item(dish_id)
    all_cart_items = get_cart_items(cart)
    if cart_item in all_cart_items:
        try:
            cart_item.delete()
        except AttributeError:
            # import pdb; pdb.set_trace()
            print('cart_item not found')
            pass
    cart.save()
    return cart


def incr_qty_in_cart(cart, dish_id):
    dish_id = int(dish_id)
    cart_item = fetch_cart_item(dish_id)
    cart_item.qty += 1
    cart_item.save()
    cart.save()
    return cart_item


def decr_qty_in_cart(cart, dish_id):
    dish_id = int(dish_id)
    cart_item = fetch_cart_item(dish_id)
    if cart_item.qty <= 1:
        cart_item.delete()
    else:
        cart_item.qty -= 1

    cart_item.save()
    cart.save()
    return cart_item


def complete_order(form, cart):
    order = form.save()
    cart_items = get_cart_items(cart)
    cart_total = get_cart_total(cart_items)
    order.total = cart_total
    order.items = cart
    order.save()
    for ordered_item in cart.ordered_items.all():
        cart.ordered_items.remove(ordered_item)
        ordered_item.delete()
    return order
