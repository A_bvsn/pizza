from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.urls import reverse

from website.models import Dish, OrderItems, Cart, Order
from website.forms import OrderForm
from website.lib.cart import cart_session
from website.lib import bl


def main_page(request):
    dishes = bl.fetch_all_dishes()
    cart = cart_session(request)
    cart_items = bl.get_cart_items(cart)
    cart_total = bl.get_cart_total(cart_items)
    cart_items_count = len(cart_items)
    context = {
        'dishes': dishes,
        'cart': cart,
        'cart_total': cart_total,
        'cart_items_count': cart_items_count,
    }
    return render(request, 'main.html', context)


def add_to_cart(request, dish_id):
    cart = cart_session(request)
    cart = bl.add_item(cart, dish_id)
    cart_items = bl.get_cart_items(cart)
    cart_total = bl.get_cart_total(cart_items)
    cart_items_count = len(cart_items)
    json_data = {
        'id': dish_id,
        'total': cart_total,
        'cart_items_count': cart_items_count,
    }
    return JsonResponse(json_data)


def remove_from_cart(request, dish_id):
    cart = cart_session(request)
    cart = bl.remove_item(cart, dish_id)
    return redirect(reverse('cart'))


def increase_quantity(request, dish_id):
    cart = cart_session(request)
    cart_item = bl.incr_qty_in_cart(cart, dish_id)
    cart_items = bl.get_cart_items(cart)
    cart_total = bl.get_cart_total(cart_items)
    qty = cart_item.qty
    json_data = {
        'id': dish_id,
        'total': cart_total,
        'quantity': qty,
    }
    return JsonResponse(json_data)


def decrease_quantity(request, dish_id):
    cart = cart_session(request)
    cart_item = bl.decr_qty_in_cart(cart, dish_id)
    cart_items = bl.get_cart_items(cart)
    cart_total = bl.get_cart_total(cart_items)
    qty = cart_item.qty
    json_data = {
        'id': dish_id,
        'total': cart_total,
        'quantity': qty,
    }
    return JsonResponse(json_data)


def cart_view(request):
    cart = cart_session(request)
    cart_items = bl.get_cart_items(cart).order_by('id')
    form = OrderForm()
    cart_total = bl.get_cart_total(cart_items)
    cart_items_count = len(cart_items)
    context = {
        'cart': cart,
        'form': form,
        'cart_items': cart_items,
        'cart_total': cart_total,
        'cart_items_count': cart_items_count,
    }
    return render(request, 'cart.html', context)


def success_view(request):
    return render(request, 'success.html')


def submit_order(request):
    cart = cart_session(request)
    cart_items = bl.get_cart_items(cart)
    cart_total = bl.get_cart_total(cart_items)
    if cart_total == 0:
        return redirect(reverse('cart'))

    form = OrderForm(request.POST)
    order = form
    context = {
        'cart': cart,
        'form': form,
    }
    if not form.is_valid():
        return redirect(reverse('cart'))

    order = bl.complete_order(form, cart)
    return redirect(reverse('success_view'))
