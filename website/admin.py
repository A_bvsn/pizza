from django.contrib import admin
from website.models import Dish, OrderItems, Cart, Order


class DishAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'image', 'price', 'kind')

class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'adress', 'phone', 'items', 'total', 'date', 'status')

admin.site.register(Dish, DishAdmin)
admin.site.register(OrderItems)
admin.site.register(Cart)
admin.site.register(Order, OrderAdmin)
