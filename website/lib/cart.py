from website.models import Cart


def cart_session(request):
    cart_id = request.session.get('cart_id', None)
    (cart, _) = Cart.objects.get_or_create(id=cart_id)
    request.session['cart_id'] = cart.id
    return cart
