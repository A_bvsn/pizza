from django.conf.urls import url
from website.views import (
    add_to_cart,
    remove_from_cart,
    main_page,
    cart_view,
    increase_quantity,
    decrease_quantity,
    success_view,
    submit_order
)


urlpatterns = [
    url(r'^$|main/$', main_page, name='main'),
    url(r'^cart/$', cart_view, name='cart'),
    url(r'^add_to_cart/(?P<dish_id>\d+)$', add_to_cart, name='add_to_cart'),
    url(r'^remove_from_cart/(?P<dish_id>\d+)$', remove_from_cart, name='remove_from_cart'),
    url(r'^increase_quantity/(?P<dish_id>\d+)$', increase_quantity, name='plus_one'),
    url(r'^decrease_quantity/(?P<dish_id>\d+)$', decrease_quantity, name='minus_one'),
    url(r'^success/$', success_view, name='success_view'),
    url(r'^submit_order/$', submit_order, name='success')
];
